m-epics-file conda recipe
=========================

Home: https://bitbucket.org/europeanspallationsource/m-epics-file

Package license: EPICS Open License

Recipe license: BSD 2-Clause

Summary: EPICS file module
